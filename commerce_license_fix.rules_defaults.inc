<?php

/**
 * @file
 * Implements hooks to introduce default rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_license_fix_default_rules_configuration() {
  /*
   * Revoke access to licenced downloads.
   */
  $export = '{ "rules_revoke_access_to_licenced_downloads" : {
    "LABEL" : "Revoke access to licenced downloads",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "commerce_license_fix" ],
      "REQUIRES" : [ "rules", "commerce_license", "entity" ],
      "ON" : { "commerce_order_update" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "commerce-order:state" ], "value" : "canceled" } },
        { "data_is" : {
          "data" : [ "commerce-order-unchanged:state" ],
            "op" : "IN",
            "value" : { "value" : { "pending" : "pending", "completed" : "completed" } }
          }
        }
      ],
      "DO" : [
        { "commerce_license_revoke_order_licenses" : { "commerce_order" : [ "commerce_order" ] } }
      ]
    }
  }';
  $configs['rules_revoke_access_to_licenced_downloads'] = entity_import('rules_config', $export);
  return $configs;
}
