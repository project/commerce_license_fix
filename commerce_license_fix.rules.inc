<?php

/**
 * @file
 * Implementation of hooks for Rules module.
 */

/**
 * Implements hook_rules_action_info_alter().
 */
function commerce_license_fix_rules_action_info_alter(&$actions) {
  $actions['commerce_license_activate_order_licenses']['label'] = t('Activates all licenses of an order');
  $actions['commerce_license_suspend_order_licenses'] = array(
    'label' => t('Suspend all licenses of an order'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce License'),
    'callbacks' => array(
      'execute' => 'commerce_license_suspend_order_licenses',
    ),
    'module' => 'commerce_license',
  );

  $actions['commerce_license_revoke_order_licenses'] = array(
    'label' => t('Revoke all licenses of an order'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce License'),
    'callbacks' => array(
      'execute' => 'commerce_license_revoke_order_licenses',
    ),
    'module' => 'commerce_license',
  );
  return $actions;
}

if (!function_exists('commerce_license_suspend_order_licenses')) {

  /**
   * Suspend all licenses of the provided order.
   *
   * @param object $order
   *   The order entity.
   */
  function commerce_license_suspend_order_licenses($order) {
    $licenses = commerce_license_get_order_licenses($order);
    foreach ($licenses as $license) {
      $license->suspend();
    }
  }

}

if (!function_exists('commerce_license_revoke_order_licenses')) {

  /**
   * Revoke all licenses of the provided order.
   *
   * @param object $order
   *   The order entity.
   */
  function commerce_license_revoke_order_licenses($order) {
    $licenses = commerce_license_get_order_licenses($order);
    foreach ($licenses as $license) {
      $license->revoke();
    }
  }

}
